# JmxOnline Api

This is the SDK part of the JMXOnline project

## Getting Started

### Prerequisites

* [Maven](https://maven.apache.org/) - Dependency Management

### Installing
open command prompt and cd to the project root

run
```
mvn clean compile assembly:single
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details