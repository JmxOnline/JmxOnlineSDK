package com.mp.vua.factories;

import com.mp.vua.services.JOnlineService;
import com.mp.vua.services.JOnlineServiceRmi;

public class JOnlineFactory {

    private JOnlineFactory() {
    }

    public static JOnlineService getInstance(){
        return new JOnlineServiceRmi();
    }
}
