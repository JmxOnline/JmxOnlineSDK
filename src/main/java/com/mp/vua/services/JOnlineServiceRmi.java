package com.mp.vua.services;

import com.google.gson.Gson;

import com.mp.vua.serverClasses.ClientRegistration;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;


import javax.management.MBeanServer;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

import java.io.IOException;
import java.lang.management.ManagementFactory;

import java.net.URL;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;


public class JOnlineServiceRmi implements JOnlineService {

    private final String USER_AGENT = "Mozilla/5.0";
    private String hostname = "localhost";
    private int port = 8009;
    private String apiKey = "";
    Registry reg = null;
    JMXConnectorServer svr;
    public void start() throws Exception {

        // Korak 1
        try{
            reg = LocateRegistry.createRegistry(port);
        }catch(RemoteException ex){
            reg = LocateRegistry.getRegistry(port);
        }


        MBeanServer mbs = ManagementFactory
                .getPlatformMBeanServer();

        
        String jmxUrl = createUrl();

        // Korak 2
        if(registerMachine(jmxUrl)){

            // Korak 3
            JMXServiceURL url = new JMXServiceURL(jmxUrl);
            svr = JMXConnectorServerFactory
                    .newJMXConnectorServer(url, null, mbs);

            svr.start();
        }else{
            throw new Exception("Invalid Jmx registration");
        }

    }

    @Override
    public void stop() throws IOException {
        if(svr != null){
            svr.stop();
        }
        if(reg != null){
            UnicastRemoteObject.unexportObject(reg,true);
        }
    }

    private boolean registerMachine(String jmxUrl) throws IOException {
        ClientRegistration registration = new ClientRegistration();
        registration.setJmxKey(apiKey);
        registration.setJmxUrl(jmxUrl);
        Gson gson = new Gson();
        String apiUrl = "http://localhost:8080/jmxOnlineApi/managedMachines/register";

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httopost = new HttpPost(apiUrl);

        StringEntity entity = new StringEntity(gson.toJson(registration));

        httopost.setEntity(entity);
        httopost.setHeader("Accept","application/json");
        httopost.setHeader("Content-type","application/json");

        CloseableHttpResponse response = client.execute(httopost);
        int responseCode = response.getStatusLine().getStatusCode();
        System.out.println("Response code "+ response.getStatusLine().getStatusCode());

        if(responseCode == 200){
            client.close();
            return true;

        }else{
            client.close();
            return false;
        }


    }

    private String createUrl() {
        StringBuilder builder = new StringBuilder();
        builder.append("service:jmx:rmi://");
        builder.append(hostname);
        builder.append("/jndi/rmi://");
        builder.append(hostname);
        builder.append(":");
        builder.append(port);
        builder.append("/jmxrmi");
        return builder.toString();
    }

    public void setUrl(String hostname,int port) {
        this.hostname = hostname;
        this.port = port;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
