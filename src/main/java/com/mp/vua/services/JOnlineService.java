package com.mp.vua.services;

import java.io.IOException;

public interface JOnlineService {

     void start() throws Exception;
     void stop() throws Exception;
     void setUrl(String hostname,int port);

     void setApiKey(String url);
}
