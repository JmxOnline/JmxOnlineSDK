package com.mp.vua.serverClasses;


public class ClientRegistration {

    private String jmxUrl;
    private String jmxKey;

    public String getJmxUrl() {
        return jmxUrl;
    }

    public void setJmxUrl(String jmxUrl) {
        this.jmxUrl = jmxUrl;
    }

    public String getJmxKey() {
        return jmxKey;
    }

    public void setJmxKey(String jmxKey) {
        this.jmxKey = jmxKey;
    }
}
